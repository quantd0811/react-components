export const stopPropagation = (event: Event): void => {
    event.stopPropagation();
}