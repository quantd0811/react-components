import { useState } from "react";

export interface DndProps {
    onDragStart: (e: any, index: number) => void;
    onDragOver: (e: any, index: number) => void;
    onDragLeave: (e: any, index: number) => void;
    onDragEnd: (e: any, index: number) => void;
}

interface UseDnd {
    data: any[];
    allowedDOMTypes: string[];
}

export const useDnd = ({ data, allowedDOMTypes }: UseDnd) => {
    const [draggedItem, setDraggedItem] = useState<any>(null);
    const [draggedItemIndex, setDraggedItemIndex] = useState<number | undefined>();
    const [draggedOverItem, setDraggedOverItem] = useState<any>(null);
    const [draggedOverItemIndex, setDraggedOverItemIndex] = useState<number | undefined>();
    const [draggedData, setDraggedData] = useState<any[]>(data);

    const onDragStart = (e: any, index: number) => {
        setDraggedItem(draggedData[index]);
        setDraggedItemIndex(index);
    };

    const onDragOver = (e: any, index: number) => {
        const currentDraggedOverItem = draggedData[index];
        setDraggedOverItem(currentDraggedOverItem);
        setDraggedOverItemIndex(index);
        // if the item is dragged over itself, ignore
        if (draggedItem === currentDraggedOverItem) return;
        const draggedOverElement = e.target.parentNode as HTMLElement;
        if (!isValidAllowedDOMTypes(draggedOverElement)) return;
        if (draggedItemIndex === undefined) return;
        if (draggedItemIndex < index)
            draggedOverElement.classList.add("dragged-down");
        else
            draggedOverElement.classList.add("dragged-up");
    }

    const onDragLeave = (e: any, index: number) => {
        const draggedLeaveElement = e.target.parentNode as HTMLElement;
        draggedLeaveElement.classList.remove("dragged-up");
        draggedLeaveElement.classList.remove("dragged-down");
    }

    const onDragEnd = (e: any, index: number) => {
        if (draggedOverItemIndex === undefined) return;
        // remove current dragged item
        let draggedDataClone = draggedData.filter((item: any, i: number) => i !== index);
        draggedDataClone.splice(draggedOverItemIndex, 0, draggedItem);
        setDraggedData(draggedDataClone);
    }

    const isValidAllowedDOMTypes = (element: any): boolean => {
        return allowedDOMTypes.filter((type: string) => element.nodeName === type.toUpperCase()).length > 0
    }

    return {
        draggedData,
        onDragStart,
        onDragOver,
        onDragLeave,
        onDragEnd
    }
}