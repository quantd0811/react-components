import { useState, useEffect } from "react";

interface UseSelect {
    data: any[];
    keyExtractor: (item: any) => any;
    onSelectedChange: (selected: any[]) => void;
    selectable?: (item: any) => boolean;
}

export const useSelect = ({
    data, keyExtractor, onSelectedChange, selectable
}: UseSelect) => {
    const allKeys = data.map(keyExtractor);
    const selectableKeys = selectable ? data.filter((item: any) => selectable(item)).map(keyExtractor) : allKeys;
    let [selected, setSelected] = useState<any[]>([]);
    const allSelected = selectableKeys.length === selected.length;
    const indeterminateSelected = selected.length > 0 && selected.length < selectableKeys.length;

    useEffect(() => {
        onSelectedChange(selected);
    }, [selected]);

    const select = (key: any): void => {
        setSelected([...selected, key]);
    }

    const selectMany = (keys: any[]): void => {
        setSelected([...selected, ...keys]);
    }

    const deselect = (key: any): void => {
        setSelected([...selected.filter((item: any) => item !== key)]);
    }

    const deselectMany = (keys: any[]): void => {
        setSelected([...selected.filter((item: any) => !keys.includes(item))]);
    }

    const isSelected = (key: any): boolean => {
        return selected.includes(key);
    }

    const toggleAll = (): void => {
        if (!allSelected || indeterminateSelected) setSelected([...selectableKeys]);
        else setSelected([]);
    }

    const initSelected = (keys: any[]): void => {
        setSelected(keys);
    }

    return {
        allSelected,
        indeterminateSelected,
        select,
        selectMany,
        deselect,
        deselectMany,
        toggleAll,
        initSelected,
        isSelected
    }
}