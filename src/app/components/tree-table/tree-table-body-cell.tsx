import React, { useState, useEffect, useContext } from 'react';
import classNames from 'classnames';
import { TreeTableBodyCellProps } from './model';
import { ArrowDown, ArrowRight } from '../../icons';
import { TreeTableContext } from './tree-table';

const TreeTableBodyCell = (props: TreeTableBodyCellProps) => {
    const {
        rowContext,
        cellIndex,
        children,
        className,
        styles,
        columnSelector,
        disableEventBubbling,
        onClick,
        onExpand
    } = props;
    const tableContext = useContext(TreeTableContext);
    const firstCellIndex = tableContext.selectable ? 1 : 0;

    const onCellClick = (event: React.MouseEvent<HTMLTableDataCellElement | MouseEvent>): void => {
        if (disableEventBubbling) event.stopPropagation();
        if (!onClick) return;
        onClick(rowContext.rowIndex, rowContext.rowLevel, columnSelector);
    }

    const cellContainerStyles = (): React.CSSProperties => {
        if (!isFirstCell()) return {};
        return {
            paddingLeft: Math.ceil(rowContext.rowLevel * 25)
        }
    }

    const isFirstCell = (): boolean => {
        return cellIndex === firstCellIndex;
    }

    const expandable = (): boolean => {
        return rowContext.rowData.chidren !== undefined && rowContext.rowData.chidren.length > 0;
    }

    const onRowExpanded = (): void => {
        if (onExpand)
            onExpand(rowContext, true);
    }

    const onRowCollapsed = (): void => {
        if (onExpand)
            onExpand(rowContext, false);
    }

    return (
        <td className={classNames(className, "tree-table-body-cell", { "clickable": onClick && onClick instanceof Function })}
            style={styles}
            onClick={onCellClick}>
            <div className="cell-container" style={cellContainerStyles()}>
                {isFirstCell() && <div className="icon-container">
                    {expandable() && rowContext.expanded && <span onClick={onRowCollapsed}><ArrowDown /></span>}
                    {expandable() && !rowContext.expanded && <span onClick={onRowExpanded}><ArrowRight /></span>}
                </div>}
                {children}
            </div>
        </td>
    )
}

export default TreeTableBodyCell;