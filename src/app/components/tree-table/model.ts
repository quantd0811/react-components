import { DndProps } from './../../hooks/useDnd';
export interface TreeTableProps {
    data: RowData[];
    children: React.ReactNode;
    theme: TableTheme;
    expandedRows: string[];
    rowClickToExpand?: boolean;
    selectRecursiveEnable?: boolean;
    selectedKeys?: string[],
    draggable?: boolean;
    selectable?: (rowData: RowData) => boolean;
    onBodyRowClick?: (rowContext: TableRowContext) => void;
    onBodyCellClick?: (rowIndex: number, rowLevel: number, columnSelector?: string) => void
}

export interface TableTheme {
    tableWrapperClass?: string;
    tableWrapperStyles?: React.CSSProperties;
    tableContainerClass?: string;
    tableContainerStyles?: React.CSSProperties;
    tableClassName?: string;
    tableStyles?: React.CSSProperties;
    headerRowClassName?: string;
    headerRowStyles?: React.CSSProperties;
    headerCellClassName?: string;
    headerCellStyles?: React.CSSProperties;
    bodyRowClassName?: string;
    bodyRowStyles?: React.CSSProperties;
    bodyCellClassName?: string;
    bodyCellStyles?: React.CSSProperties;
}

export interface TreeTableColumnProps {
    selector?: string;
    label?: React.ReactNode;
    sortable?: boolean;
    className?: string;
    styles?: React.CSSProperties;
    children?: (rowContext: TableRowContext) => React.ReactNode;
}

export interface TreeTableHeaderCellProps {
    children?: React.ReactNode;
    icon?: React.ReactNode;
    iconPosition?: "left" | "right";
    sortable?: boolean;
    className?: string;
    styles?: React.CSSProperties;
}

export interface TreeTableHeaderRowProps {
    onClick?: () => void;
    className?: string;
    styles?: React.CSSProperties;
    children: React.ReactNode
}

export interface RowData {
    key: string;
    data: any;
    parentKey?: any;
    chidren?: RowData[];
}

export interface TreeTableBodyRowProps {
    rowContext: TableRowContext;
    onClick?: (rowContext: TableRowContext) => void;
    className?: string;
    draggeble?: boolean;
    dndProps?: DndProps;
    styles?: React.CSSProperties;
    children: React.ReactNode;
}

export interface TreeTableBodyCellProps {
    rowContext: TableRowContext;
    cellIndex: number;
    children?: React.ReactNode;
    className?: string;
    styles?: React.CSSProperties;
    columnSelector?: string;
    disableEventBubbling?: boolean;
    onClick?: (rowIndex: number, rowLevel: number, columnSelector?: string) => void
    onExpand?: (rowContext: TableRowContext, expanded: boolean) => void;
}

export interface TableRowContext {
    rowKey: string;
    rowData: RowData;
    rowIndex: number;
    rowLevel: number;
    expanded: boolean;
}

export interface TableContext {
    selectable?: (rowData: RowData) => boolean;
}