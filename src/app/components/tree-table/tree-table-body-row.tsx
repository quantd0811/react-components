import React from 'react';
import { TreeTableBodyRowProps } from './model';
import classNames from 'classnames';

const TreeTableBodyRow = (props: TreeTableBodyRowProps) => {
    const {
        rowContext,
        onClick,
        className,
        styles,
        draggeble,
        dndProps,
        children
    } = props;

    const onRowClick = (): void => {
        if (!onClick) return;
        onClick(rowContext);
    }

    return (
        <tr className={classNames(className, "tree-table-body-row",
            { "clickable": onClick && onClick instanceof Function, "expanded": rowContext.expanded })}
            style={styles}
            onClick={onRowClick}
            draggable={draggeble}
            onDragStart={(e: any) => {
                if (!draggeble) return;
                if (dndProps && dndProps.onDragStart)
                    dndProps.onDragStart(e, rowContext.rowIndex);
            }}
            onDragOver={(e) => {
                if (!draggeble) return;
                if (dndProps && dndProps.onDragOver)
                    dndProps.onDragOver(e, rowContext.rowIndex);
            }}
            onDragLeave={(e) => {
                if (!draggeble) return;
                if (dndProps && dndProps.onDragLeave)
                    dndProps.onDragLeave(e, rowContext.rowIndex);
            }}
            onDragEnd={(e) => {
                if (!draggeble) return;
                if (dndProps && dndProps.onDragEnd)
                    dndProps.onDragEnd(e, rowContext.rowIndex);
            }}>
            {children}
        </tr>
    )
}

export default TreeTableBodyRow;