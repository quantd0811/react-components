import React, { useState, useEffect } from 'react';
import './styles/main.scss';
import { TreeTableColumnProps, RowData, TreeTableProps, TableTheme, TableRowContext, TableContext } from './model';
import TreeTableHeaderRow from './tree-table-header-row';
import TreeTableHeaderCell from './tree-table-header-cell';
import TreeTableBodyRow from './tree-table-body-row';
import TreeTableBodyCell from './tree-table-body-cell';
import classNames from 'classnames';
import { Checkbox } from './../../components';
import { useSelect, useDnd } from '../../hooks';

function Col(_: TreeTableColumnProps) {
    return null as unknown as JSX.Element;
}

export const TreeTableContext = React.createContext({} as TableContext);

const TreeTable = (props: TreeTableProps) => {
    const {
        data,
        children,
        theme,
        expandedRows,
        rowClickToExpand,
        selectRecursiveEnable,
        selectable,
        selectedKeys,
        draggable,
        onBodyRowClick,
        onBodyCellClick
    } = props;
    const columns = React.Children.toArray(children)
        .filter((child) => (child as React.ReactElement).type === Col)
        .map((child) => (child as React.ReactElement).props as TreeTableColumnProps);
    const [expandedNodes, setExpandedNodes] = useState<string[]>(expandedRows);
    const flattenData = (): RowData[] => {
        let allRowDatas = [] as RowData[];
        data.forEach((rowData: RowData) => {
            allRowDatas.push(rowData);
            allRowDatas.push(...getAllChildrenRecursive(rowData));
        });
        return allRowDatas;
    }

    // helpers
    const isExpanded = (rowKey: string): boolean => {
        return expandedNodes.includes(rowKey);
    }

    const getAllChildrenRecursive = (root: RowData): RowData[] => {
        if (!root.chidren || root.chidren.length === 0) return [];
        let children = [...root.chidren] as RowData[];
        root.chidren.forEach((child: RowData) => {
            children.push(...getAllChildrenRecursive(child));
        })
        return children;
    }

    const expandRow = (rowContext: TableRowContext): void => {
        setExpandedNodes([...expandedNodes, rowContext.rowKey]);
    }

    const collapseRow = (rowContext: TableRowContext): void => {
        let allChildrenKeys = getAllChildrenRecursive(rowContext.rowData)
            .map((child: RowData) => child.key);
        setExpandedNodes(expandedNodes.filter((node: string) => node !== rowContext.rowKey &&
            !allChildrenKeys.includes(node)));
    }

    const selectRecursive = (rowContext: TableRowContext): void => {
        selectMany([rowContext.rowKey, ...getAllChildrenRecursive(rowContext.rowData).map((child: RowData) => child.key)]);
    }

    const deselectRecursive = (rowContext: TableRowContext): void => {
        deselectMany([rowContext.rowKey, ...getAllChildrenRecursive(rowContext.rowData).map((child: RowData) => child.key)]);
    }

    const initSelectRecursive = (rowKeys: string[]): void => {
        let rowDatas = flattenData().filter((row: RowData) => rowKeys.includes(row.key));
        if (!rowDatas || rowDatas.length === 0) return;
        let selectedItems = [] as string[];
        rowDatas.forEach((rowData: RowData) => {
            selectedItems.push(...[rowData.key, ...getAllChildrenRecursive(rowData).map((child: RowData) => child.key)]);
        })
        initSelected(selectedItems);
    }

    // event
    const onExpand = (rowContext: TableRowContext, expanding: boolean): void => {
        if (expanding) expandRow(rowContext);
        else collapseRow(rowContext);
    }

    const onRowClick = (rowContext: TableRowContext): void => {
        if (onBodyRowClick) onBodyRowClick(rowContext);
        if (rowClickToExpand) onExpand(rowContext, !rowContext.expanded);
    }

    const onSelectedChange = (selectedKeys: string[]): void => {
        console.log(selectedKeys);
    }

    // use select
    const keyExtractor = (rowData: RowData) => rowData.key;
    const {
        allSelected, deselect, isSelected, select, toggleAll, selectMany, deselectMany, initSelected
    } = useSelect({ data: flattenData(), keyExtractor, onSelectedChange, selectable });

    const {
        draggedData, onDragOver, onDragStart, onDragLeave, onDragEnd
    } = useDnd({ data, allowedDOMTypes: ["tr"] });

    useEffect(() => {
        debugger
        if (!selectedKeys) return;
        if (!selectRecursiveEnable) { initSelected(selectedKeys); return; };
        initSelectRecursive(selectedKeys);
    }, [selectedKeys]);

    const renderHeaderRow = (): React.ReactNode => {
        return <TreeTableHeaderRow
            className={theme.headerRowClassName}
            styles={theme.headerRowStyles}>
            {selectable && <TreeTableHeaderCell
                key={"select-all"}
                className={theme.headerCellClassName}
                styles={theme.headerCellStyles}>
                <Checkbox
                    checked={allSelected}
                    onChange={toggleAll} />
            </TreeTableHeaderCell>}
            {columns.map((col: TreeTableColumnProps, index: number) => {
                return <TreeTableHeaderCell
                    key={index}
                    className={theme.headerCellClassName}
                    styles={theme.headerCellStyles}>{col.label}</TreeTableHeaderCell>
            })}
        </TreeTableHeaderRow>
    }

    const renderBodyRow = (rowData: RowData, rowIndex: number, rowLevel: number, visible: boolean): React.ReactNode => {
        const rowContext = {
            rowKey: rowData.key,
            rowData, rowIndex, rowLevel,
            expanded: isExpanded(rowData.key)
        } as TableRowContext;
        rowLevel++;
        return (
            <>
                <TreeTableBodyRow
                    key={rowContext.rowKey}
                    rowContext={rowContext}
                    className={classNames(theme.bodyRowClassName, {
                        "visible": visible,
                        "mdc-data-table__row--selected": isSelected(rowContext.rowKey)
                    })}
                    styles={theme.bodyRowStyles}
                    onClick={onRowClick}
                    draggeble
                    dndProps={{
                        onDragStart: onDragStart,
                        onDragOver: onDragOver,
                        onDragLeave: onDragLeave,
                        onDragEnd: onDragEnd
                    }}>
                    {selectable && <TreeTableBodyCell
                        key={0}
                        rowContext={rowContext}
                        cellIndex={0}
                        className={classNames(theme.bodyCellClassName, "checkbox-cell")}
                        styles={theme.bodyCellStyles}
                        disableEventBubbling>
                        <Checkbox
                            checked={isSelected(rowContext.rowKey)}
                            onChange={(checked: boolean) => {
                                if (checked) {
                                    if (selectRecursiveEnable) selectRecursive(rowContext);
                                    else select(rowContext.rowKey)
                                }
                                else {
                                    if (selectRecursiveEnable) deselectRecursive(rowContext);
                                    else deselect(rowContext.rowKey)
                                }
                            }}
                            disable={!selectable(rowContext.rowData)} />
                    </TreeTableBodyCell>}
                    {columns.map((col: TreeTableColumnProps, index: number) => {
                        index = selectable ? index + 1 : index;
                        return <TreeTableBodyCell
                            key={index}
                            rowContext={rowContext}
                            cellIndex={index}
                            columnSelector={col.selector}
                            className={theme.bodyCellClassName}
                            styles={theme.bodyCellStyles}
                            onClick={onBodyCellClick}
                            onExpand={onExpand}
                        >
                            {!col.children && col.selector ? rowData.data[col.selector] : ""}
                            {col.children && col.children instanceof Function && col.children(rowContext)}
                        </TreeTableBodyCell>
                    })}
                </TreeTableBodyRow>
                {rowData.chidren?.map((item: RowData, index: number) => renderBodyRow(item, index, rowLevel, rowContext.expanded))}
            </>
        )
    }

    return (
        <TreeTableContext.Provider value={{
            selectable: selectable
        }}>
            <div className={theme.tableWrapperClass} style={theme.tableWrapperStyles}>
                <div className={classNames(theme.tableContainerClass, "tree-table-container")} style={theme.tableContainerStyles}>
                    <table className={classNames(theme.tableClassName, "tree-table")} style={theme.tableStyles}>
                        <thead>
                            {renderHeaderRow()}
                        </thead>
                        <tbody>
                            {!draggable && data.map((item, index: number) => renderBodyRow(item, index, 0, true))}
                            {draggable && draggedData.map((item, index: number) => renderBodyRow(item, index, 0, true))}
                        </tbody>
                    </table>
                </div>
            </div>
        </TreeTableContext.Provider>
    )
}

TreeTable.Col = Col;
TreeTable.defaultProps = {
    expandedRows: [],
    selectedKeys: [],
    theme: {
        tableWrapperClass: "mdc-data-table",
        tableContainerClass: "mdc-data-table__table-container",
        tableClassName: "mdc-data-table__table",
        headerRowClassName: "mdc-data-table__row",
        headerCellClassName: "mdc-data-table__header-cell",
        bodyRowClassName: "mdc-data-table__row",
        bodyCellClassName: "mdc-data-table__cell"
    } as TableTheme
}

export default TreeTable;