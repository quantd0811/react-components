import React from 'react';
import classNames from 'classnames';
import { TreeTableHeaderCellProps } from './model';

const TreeTableHeaderCell = (props: TreeTableHeaderCellProps) => {
    const {
        children,
        icon,
        iconPosition,
        sortable,
        className,
        styles
    } = props;
    return (
        <th className={classNames(className, "tree-table-header-cell")}
            role="columnheader"
            scope="col" 
            style={styles}>
            {children}
        </th>
    )
}

export default TreeTableHeaderCell;