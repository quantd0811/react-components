import React from 'react';
import classNames from 'classnames';
import { TreeTableHeaderRowProps } from './model';

const TreeTableHeaderRow = (props: TreeTableHeaderRowProps) => {
    const {
        onClick,
        className,
        styles,
        children
    } = props;

    return (
        <tr className={classNames(className, "tree-table-header-row")}
            style={styles}
            onClick={onClick}>
            {children}
        </tr>
    )
}

TreeTableHeaderRow.defaultProps = {
    rowLevel: 0
}

export default TreeTableHeaderRow;