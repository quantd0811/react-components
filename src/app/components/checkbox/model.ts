export interface CheckboxProps {
    checked: boolean;
    disable?: boolean;
    indetermiate?: boolean;
    onChange: (checked: boolean) => void;
    className?: string;
    styles?: React.CSSProperties;
}