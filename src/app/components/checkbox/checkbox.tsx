import React, { useRef, useEffect } from 'react';
import { CheckboxProps } from './model';
import classNames from 'classnames';
import "./styles/main.scss";

const Checkbox = (props: CheckboxProps) => {
    const {
        checked,
        onChange,
        className,
        disable,
        indetermiate,
        styles
    } = props;
    const inputRef = useRef<HTMLInputElement>();

    const handleChange = (event: any): void => {
        onChange(event.target.checked);
    }

    useEffect(() => {
        if (inputRef && inputRef.current)
            inputRef.current.indeterminate = Boolean(indetermiate);
    }, [indetermiate])

    return (
        <div className={classNames("mdc-checkbox", {
            "is-disabled": disable
        }, className)}
            style={styles}>
            <input
                type="checkbox"
                className="mdc-checkbox__native-control"
                checked={checked}
                disabled={disable}
                onChange={handleChange} />
            <div className="mdc-checkbox__background">
                <svg className="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                    <path className="mdc-checkbox__checkmark-path" fill="none" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                </svg>
                <div className="mdc-checkbox__mixedmark"></div>
            </div>
            <div className="mdc-checkbox__ripple"></div>
        </div>
    )
}

export default Checkbox;