import React from 'react';
import './app/styles/main.scss';
import { TreeTable } from './app/components/tree-table';
import { TableRowContext, RowData } from 'app/components/tree-table/model';

function App() {
  return (
    <div className="App">
      <TreeTable data={[
        {
          key: "1",
          data: { name: "Item 1", description: "Desc 1" },
          chidren: [
            {
              key: "1.1",
              data: { name: "Item 1.1", description: "Desc 1.1" },
            },
            {
              key: "1.2",
              data: { name: "Item 1.2", description: "Desc 1.2" },
              chidren: [
                {
                  key: "1.2.1",
                  data: { name: "Item 1.2.1", description: "Desc 1.2.1" },
                },
                {
                  key: "1.2.2",
                  data: { name: "Item 1.2.2", description: "Desc 1.2.2" },
                }
              ]
            }
          ]
        },
        {
          key: "2",
          data: { name: "Item 2", description: "Desc 2" },
        },
        {
          key: "3",
          data: { name: "Item 3", description: "Desc 3" },
          chidren: [
            {
              key: "3.1",
              data: { name: "Item 3.1", description: "Desc 3.1" },
            }
          ]
        }
      ]}
        // onBodyRowClick={(rowContext: TableRowContext) => {
        //   console.log(rowContext);
        // }}
        selectable={(rowData: RowData) => true}
        draggable>
        <TreeTable.Col label="Name" selector="name"></TreeTable.Col>
        <TreeTable.Col label="Description" selector="description"></TreeTable.Col>
        <TreeTable.Col>
          {(rowContext: TableRowContext) => (
            <div>{rowContext.rowIndex}</div>
          )}
        </TreeTable.Col>
      </TreeTable>
    </div>
  );
}

export default App;
